import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { FormWizardModule } from 'angular2-wizard';
import { AgmCoreModule } from '@agm/core';
import { LandingComponent } from './landing/landing.component';
import { ContactComponent } from './contact/contact.component';
import { DriverComponent } from './driver/driver.component';
import { CustomerComponent } from './customer/customer.component';
import { DispatchingComponent } from './dispatching/dispatching.component';

@NgModule({
  declarations: [LandingComponent, ContactComponent, DriverComponent, CustomerComponent, DispatchingComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    FormWizardModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDBJuB4fWBQdMw3-ygxuPADoNm8cDIMuBM'
    })
  ]
})
export class MainModule { }
