import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  zoom: number = 8;
  lat: number = 51.673858;
  lng: number = 7.815982;
  constructor() { }
  ngOnInit() {
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
	  {
		  lat: 51.673858,
		  lng: 7.815982,
		  draggable: true
	  },
	  {
		  lat: 51.373858,
		  lng: 7.215982,
		
		  draggable: false
	  },
	  {
		  lat: 51.723858,
		  lng: 7.895982,
		  
		  draggable: true
    },
    {
		  lat: 51.5063858,
		  lng: 7.825982,
		  draggable: true
	  },
	  {
		  lat: 51.213858,
		  lng: 7.315982,
		
		  draggable: false
	  },
	  {
		  lat: 51.843858,
		  lng: 7.905982,
		  
		  draggable: true
	  }
  ]
}
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
  


