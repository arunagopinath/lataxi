import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LandingComponent} from './landing/landing.component';
import { ContactComponent } from './contact/contact.component';
import { DriverComponent } from './driver/driver.component';
import { CustomerComponent } from './customer/customer.component';
import { DispatchingComponent } from './dispatching/dispatching.component';

const routes: Routes = [
    { path:"home", component: LandingComponent},
    { path:"contact" , component:ContactComponent},
    { path:"driver", component:DriverComponent},
    { path:"customer", component:CustomerComponent},
    { path:"dispatch", component:DispatchingComponent}
    
    
    ];

@NgModule({
    imports: [RouterModule.forRoot(routes),
    CommonModule
  ],
    exports: [RouterModule]
  })
export class MainRoutingModule { }