import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { from } from 'rxjs';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(public router: Router) { }
  gotoDriver(){
this.router.navigate(['/driver']);
  }
  gotoPassenger(){
    this.router.navigate(['/customer']);
      }
      gotoContact(){
        this.router.navigate(['/contact']);
          }
          gotoDispatching(){
            this.router.navigate(['/dispatch']);
              }
    
  ngOnInit() {
  }

}
